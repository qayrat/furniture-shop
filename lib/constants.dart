import 'package:flutter/material.dart';

class AppColors {
  static const colorPrimary = Color(0xFFF9C678);
  static const colorSecondary = Color(0xFFFF2D55);
  static const colorBlack = Color(0xFF242A37);
  static const colorGrey = Color(0xFFF1F2F6);
}

const double defaultPadding = 16.0;
const double defaultBorderRadius = 12.0;
