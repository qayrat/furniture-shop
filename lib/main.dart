import 'package:flutter/material.dart';

import 'models/furniture_type.dart';
import 'screens/category/category_page.dart';
import 'screens/category/widgets/category_item_page.dart';
import 'screens/home/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Furniture Shop',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case '/category':
            return MaterialPageRoute(builder: (context) => CategoryPage());
          case '/categoryItem':
            FurnitureType object = settings.arguments as FurnitureType;
            return MaterialPageRoute(
              builder: (context) => CategoryItemPage(furnitureType: object),
            );
        }
        return null;
      },
    );
  }
}
