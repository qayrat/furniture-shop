import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:furniture_shop/constants.dart';
import 'package:furniture_shop/screens/home/widgets/new_arrivals_section.dart';

import 'widgets/home_furniture_type.dart';
import 'widgets/section_title.dart';
import 'widgets/trending_section.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        leading: Builder(
          builder: ((context) => IconButton(
                onPressed: () {
                  Scaffold.of(context).openDrawer();
                },
                icon: SvgPicture.asset('assets/icons/menu.svg'),
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              )),
        ),
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark,
        ),
        actions: [
          IconButton(
            autofocus: true,
            onPressed: () {},
            icon: const Icon(Icons.search, color: AppColors.colorBlack),
          ),
          IconButton(
            autofocus: true,
            onPressed: () {},
            icon: const Icon(
              Icons.shopping_bag_outlined,
              color: AppColors.colorBlack,
            ),
          )
        ],
      ),
      drawer: Drawer(
        child: ListView(
          padding: const EdgeInsets.all(defaultPadding),
          children: [
            const DrawerHeader(
              child: Center(
                child: ListTile(
                  title: Text(
                    'Elon Mask',
                    style: TextStyle(fontSize: 16.0),
                  ),
                  subtitle: Text(
                    'elonmusk@gmail.com',
                    style: TextStyle(fontSize: 13.0),
                  ),
                  leading: CircleAvatar(
                    backgroundImage: AssetImage('assets/images/elon_mask.jpg'),
                    radius: 30.0,
                  ),
                ),
              ),
            ),
            ListTile(
              title: const Text('Categories'),
              leading: const Icon(Icons.category, color: Colors.grey),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/category');
              },
            ),
            ListTile(
              title: const Text('New collections'),
              leading: const Icon(Icons.collections, color: Colors.grey),
              onTap: () {},
            ),
            ListTile(
              title: const Text('Editor\'s Picks'),
              leading: const Icon(Icons.add_box, color: Colors.grey),
              onTap: () {},
            ),
            ListTile(
              title: const Text('Notifications'),
              leading: const Icon(Icons.notifications, color: Colors.grey),
              onTap: () {},
            ),
            ListTile(
              title: const Text('Settings'),
              leading: const Icon(Icons.settings, color: Colors.grey),
              onTap: () {},
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Padding(
                padding: EdgeInsets.only(
                  left: 16.0,
                  right: 16.0,
                  top: 20.0,
                  bottom: 16.0,
                ),
                child: Text(
                  'Discover',
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              HomeFurnitureType(),
              const SizedBox(height: 10.0),
              SectionTitle(title: 'Trending', press: () {}),
              TrendingSection(),
              const SizedBox(height: 10.0),
              SectionTitle(title: 'New Arrivals', press: () {}),
              NewArrivalsSection(),
              const SizedBox(height: 100.0),
            ],
          ),
        ),
      ),
    );
  }
}
