import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:furniture_shop/constants.dart';

class HomeBanner extends StatelessWidget {
  const HomeBanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 300,
      width: double.infinity,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset(
            'assets/images/home_pager3.jpg',
            fit: BoxFit.cover,
          ),
          InkWell(
            autofocus: true,
            onTap: () {
              log('banner tap');
            },
            child: Container(
              margin: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: const [
                  Text(
                    'Cyber Monday',
                    style: TextStyle(
                      fontSize: 15,
                      color: AppColors.colorBlack,
                    ),
                  ),
                  Text(
                    'Sale Up To 70% Off',
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: AppColors.colorBlack,
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 80,
            child: AppBar(
              shadowColor: Colors.transparent,
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              leading: IconButton(
                onPressed: () {},
                icon: SvgPicture.asset('assets/icons/menu.svg'),
              ),
              systemOverlayStyle: const SystemUiOverlayStyle(
                  statusBarColor: Colors.transparent,
                  statusBarIconBrightness: Brightness.dark),
              actions: [
                IconButton(
                  autofocus: true,
                  onPressed: () {
                    log('appBar');
                  },
                  icon: const Icon(Icons.search, color: Colors.black),
                ),
                IconButton(
                  autofocus: true,
                  onPressed: () {
                    log('appBar');
                  },
                  icon: const Icon(Icons.shopping_bag_outlined,
                      color: Colors.black),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
