import 'package:flutter/material.dart';
import 'package:furniture_shop/constants.dart';

class TrendingSection extends StatelessWidget {
  const TrendingSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: const [
        TrendingItem(
          image: 'assets/images/beds.png',
          name: 'Black and White bed',
          price: '52.00',
        ),
        Padding(
          padding: EdgeInsets.only(left: 112),
          child: Divider(thickness: 1.5),
        ),
        TrendingItem(
          image: 'assets/images/sofa.png',
          name: 'Ovora Design Table Teak',
          price: '67.99',
        ),
        Padding(
          padding: EdgeInsets.only(left: 112),
          child: Divider(thickness: 1.5),
        ),
        TrendingItem(
          image: 'assets/images/chair.png',
          name: 'Ivonne chair yellow',
          price: '24.00',
        ),
      ],
    );
  }
}

class TrendingItem extends StatelessWidget {
  const TrendingItem({
    Key? key,
    required this.image,
    required this.name,
    required this.price,
  }) : super(key: key);

  final String image;
  final String name;
  final String price;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      margin: const EdgeInsets.only(left: 16.0, right: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            height: double.infinity,
            width: 80,
            padding: const EdgeInsets.all(defaultPadding / 2),
            decoration: BoxDecoration(
              color: AppColors.colorGrey,
              borderRadius: BorderRadius.circular(defaultBorderRadius),
            ),
            child: Image.asset(image),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                const SizedBox(height: defaultPadding),
                Text(
                  name,
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 15.0,
                  ),
                ),
                const SizedBox(height: 10.0),
                Text(
                  '\$$price',
                  style: const TextStyle(
                    fontSize: 12.0,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
