import 'package:flutter/material.dart';
import 'package:furniture_shop/constants.dart';
import 'package:furniture_shop/models/resource.dart';

class NewArrivalsSection extends StatelessWidget {
  NewArrivalsSection({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 210,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 4,
        itemBuilder: ((context, index) {
          return Container(
            margin: EdgeInsets.only(left: (index == 0 ? 16.0 : 0), right: 16.0),
            width: 140,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 140,
                  width: double.infinity,
                  padding: const EdgeInsets.all(defaultPadding / 2),
                  decoration: BoxDecoration(
                    color: AppColors.colorGrey,
                    borderRadius: BorderRadius.circular(defaultBorderRadius),
                  ),
                  child: Image.asset(
                    Resources.furnitures[index].image,
                  ),
                ),
                const SizedBox(height: 10),
                Text(
                  Resources.furnitures[index].name,
                  style: const TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 15.0,
                  ),
                ),
                const SizedBox(height: 2),
                Text(
                  '\$${Resources.furnitures[index].price}',
                  style: const TextStyle(
                    fontSize: 12.0,
                    color: Colors.grey,
                  ),
                ),
              ],
            ),
          );
        }),
      ),
    );
  }
}
