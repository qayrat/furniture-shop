import 'package:flutter/material.dart';
import 'package:furniture_shop/constants.dart';
import 'package:furniture_shop/models/furniture_type.dart';

class HomeFurnitureType extends StatelessWidget {
  HomeFurnitureType({Key? key}) : super(key: key);

  final PageController pageController = PageController();

  final List<String> categories = ['Featured', 'Collection', 'Trend'];
  final List<FurnitureType> furnitures = [
    FurnitureType(
        id: 1, name: 'Chairs', image: 'assets/images/chair.png', count: 1146),
    FurnitureType(
        id: 2, name: 'Tables', image: 'assets/images/table.png', count: 442),
    FurnitureType(
        id: 3, name: 'Sofa', image: 'assets/images/sofa.png', count: 1866),
    FurnitureType(
        id: 4, name: 'Beds', image: 'assets/images/beds.png', count: 256),
  ];

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 240.0,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 4,
        itemBuilder: ((context, index) {
          return Container(
            margin: EdgeInsets.only(left: (index == 0 ? 16.0 : 0), right: 16.0),
            width: 140,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  flex: 10,
                  child: Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(defaultPadding / 2),
                    decoration: BoxDecoration(
                      color: AppColors.colorGrey,
                      borderRadius: BorderRadius.circular(defaultBorderRadius),
                    ),
                    child: Image.asset(
                      furnitures[index].image,
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Expanded(
                  flex: 1,
                  child: Text(
                    furnitures[index].name,
                    style: const TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 15.0,
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Text(
                    '${furnitures[index].count} items',
                    style: const TextStyle(
                      fontSize: 12.0,
                      color: Colors.grey,
                    ),
                  ),
                ),
              ],
            ),
          );
        }),
      ),
    );
  }
}
