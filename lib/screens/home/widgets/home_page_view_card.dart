import 'package:flutter/material.dart';
import 'package:furniture_shop/constants.dart';

class HomePageViewCard extends StatelessWidget {
  const HomePageViewCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Image.asset(
            'assets/images/home_pager3.jpg',
            fit: BoxFit.cover,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Cyber Monday',
                style: TextStyle(color: AppColors.colorBlack),
              ),
              const Text(
                'Sale Up To 70% Off',
                style: TextStyle(
                  color: AppColors.colorBlack,
                  fontSize: 30.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
