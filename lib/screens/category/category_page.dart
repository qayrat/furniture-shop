import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:furniture_shop/constants.dart';
import 'package:furniture_shop/models/furniture_type.dart';
import 'package:furniture_shop/models/resource.dart';

class CategoryPage extends StatelessWidget {
  CategoryPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Categories',
          style: TextStyle(color: AppColors.colorBlack),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark,
        ),
        iconTheme: const IconThemeData(color: AppColors.colorBlack),
      ),
      body: Container(
        color: Colors.white,
        child: ListView.separated(
          itemBuilder: ((context, index) {
            return CategoryItem(furnitureType: Resources.furnitureTypes[index]);
          }),
          separatorBuilder: (context, index) => const Padding(
            padding: EdgeInsets.only(left: 16.0),
            child: Divider(thickness: 1.5),
          ),
          itemCount: Resources.furnitureTypes.length,
        ),
      ),
    );
  }
}

class CategoryItem extends StatelessWidget {
  const CategoryItem({
    Key? key,
    required this.furnitureType,
  }) : super(key: key);

  final FurnitureType furnitureType;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (() {
        Navigator.pushNamed(context, '/categoryItem', arguments: furnitureType);
      }),
      child: Container(
        height: 100,
        margin: const EdgeInsets.only(left: 16.0, right: 16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              height: double.infinity,
              width: 100,
              padding: const EdgeInsets.all(defaultPadding / 2),
              decoration: BoxDecoration(
                color: AppColors.colorGrey,
                borderRadius: BorderRadius.circular(defaultBorderRadius),
              ),
              child: Image.asset(furnitureType.image),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  const SizedBox(height: defaultPadding),
                  Text(
                    furnitureType.name,
                    style: const TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 17.0,
                    ),
                  ),
                  const SizedBox(height: 10.0),
                  Text(
                    '${furnitureType.count} items',
                    style: const TextStyle(
                      fontSize: 14.0,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
