import 'package:flutter/material.dart';
import 'package:furniture_shop/constants.dart';
import 'package:furniture_shop/models/furniture.dart';

class CategoryItem extends StatelessWidget {
  const CategoryItem({
    Key? key,
    required this.furniture,
    required this.index,
  }) : super(key: key);
  final Furniture furniture;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        left: (index % 2 == 0 ? 16.0 : 0),
        right: (index % 2 != 0 ? 16.0 : 0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 10,
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.all(defaultPadding / 2),
              decoration: BoxDecoration(
                color: AppColors.colorGrey,
                borderRadius: BorderRadius.circular(defaultBorderRadius),
              ),
              child: Image.asset(
                furniture.image,
              ),
            ),
          ),
          const SizedBox(height: 10),
          Expanded(
            flex: 2,
            child: Text(
              furniture.name,
              style: const TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 15.0,
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              '\$${furniture.price}',
              style: const TextStyle(
                fontSize: 12.0,
                color: Colors.grey,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
