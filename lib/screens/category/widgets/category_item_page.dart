import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:furniture_shop/models/furniture_type.dart';
import 'package:furniture_shop/models/resource.dart';
import 'package:furniture_shop/screens/category/widgets/category_item.dart';
import 'package:furniture_shop/screens/category/widgets/filter_dialog_page.dart';

import '../../../constants.dart';

class CategoryItemPage extends StatefulWidget {
  const CategoryItemPage({Key? key, required this.furnitureType})
      : super(key: key);

  final FurnitureType furnitureType;

  @override
  State<CategoryItemPage> createState() =>
      _CategoryItemPageState(furnitureType: furnitureType);
}

class _CategoryItemPageState extends State<CategoryItemPage> {
  final FurnitureType furnitureType;

  _CategoryItemPageState({required this.furnitureType});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    // final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    const double itemHeight = 300;
    final double itemWidth = size.width / 2;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          furnitureType.name,
          style: const TextStyle(color: AppColors.colorBlack),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark,
        ),
        iconTheme: const IconThemeData(color: AppColors.colorBlack),
        actions: [
          TextButton(
            onPressed: (() {
              _openFilterDialog();
            }),
            child: const Text(
              'Filters',
              style: TextStyle(color: AppColors.colorBlack),
            ),
          )
        ],
      ),
      body: Container(
        color: Colors.white,
        child: MediaQuery.removePadding(
          context: context,
          removeLeft: true,
          removeRight: true,
          child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: 16,
                mainAxisSpacing: 16,
                crossAxisCount: 2,
                childAspectRatio: (itemWidth / itemHeight),
              ),
              itemCount: Resources.furnitures.length,
              itemBuilder: (BuildContext context, int index) {
                return CategoryItem(
                    furniture: Resources.furnitures[index], index: index);
              }),
        ),
      ),
    );
  }

  void _openFilterDialog() {
    showGeneralDialog(
      context: context,
      pageBuilder: (context, animation1, animation2) {
        return FilterDialogPage();
      },
    );
  }
}
