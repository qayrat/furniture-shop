import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import '../../../constants.dart';

class FilterDialogPage extends StatefulWidget {
  const FilterDialogPage({Key? key}) : super(key: key);

  @override
  State<FilterDialogPage> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<FilterDialogPage> {
  RangeValues _currentRangeValues = const RangeValues(50, 1500);

  @override
  Widget build(BuildContext context) {
    var filterCount = 0;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.close),
        ),
        backgroundColor: Colors.white,
        elevation: 0.0,
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark,
        ),
        iconTheme: const IconThemeData(color: AppColors.colorBlack),
      ),
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Expanded(
            flex: 10,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: defaultPadding),
                    const Text(
                      'Filters',
                      style: TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.w500,
                        color: AppColors.colorBlack,
                      ),
                    ),
                    const SizedBox(height: defaultPadding / 2),
                    Divider(),
                    const SizedBox(height: defaultPadding / 2),
                    const Text(
                      'Filter By Price',
                      style: TextStyle(
                        fontSize: 16,
                        color: AppColors.colorBlack,
                      ),
                    ),
                    const SizedBox(height: defaultPadding),
                    RangeSlider(
                      activeColor: AppColors.colorBlack,
                      inactiveColor: Colors.grey[400],
                      min: 0,
                      max: 2000,
                      values: _currentRangeValues,
                      labels: RangeLabels(
                        '${_currentRangeValues.start}',
                        '${_currentRangeValues.end}',
                      ),
                      onChanged: (RangeValues newRange) {
                        setState(() {
                          _currentRangeValues = newRange;
                        });
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          '\$${_priceFormat(_currentRangeValues.start)}',
                          style: const TextStyle(
                            color: AppColors.colorBlack,
                          ),
                        ),
                        Text(
                          '\$${_priceFormat(_currentRangeValues.end)}',
                          style: const TextStyle(
                            color: AppColors.colorBlack,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 40),
                    const Text(
                      'Brand',
                      style: TextStyle(
                        fontSize: 16,
                        color: AppColors.colorBlack,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              color: AppColors.colorBlack,
              width: double.infinity,
              child: Center(
                child: TextButton(
                  onPressed: () {},
                  style: TextButton.styleFrom(
                    primary: Colors.white,
                    backgroundColor: AppColors.colorBlack,
                  ),
                  child: Text('Apply'.toUpperCase()),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  String _priceFormat(double start) {
    return NumberFormat("###0.0#", "en_US").format(start);
  }
}
