class FurnitureType {
  final int id;
  final String name;
  final String image;
  final int count;
  final List<String>? categories;

  FurnitureType({
    required this.id,
    required this.name,
    required this.image,
    required this.count,
    this.categories,
  });
}
