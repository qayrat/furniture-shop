import 'furniture.dart';
import 'furniture_type.dart';

class Resources {
  static final List<String> categories = ['Featured', 'Collection', 'Trend'];
  static final List<FurnitureType> furnitureTypes = [
    FurnitureType(
      id: 1,
      name: 'Ceiling',
      image: 'assets/images/ceiling_lamp.png',
      count: 1242,
      categories: categories,
    ),
    FurnitureType(
      id: 2,
      name: 'Lamps',
      image: 'assets/images/lamp.png',
      count: 1455,
      categories: categories,
    ),
    FurnitureType(
      id: 3,
      name: 'Furniture',
      image: 'assets/images/chair.png',
      count: 735,
      categories: categories,
    ),
    FurnitureType(
        id: 4,
        name: 'Beds',
        image: 'assets/images/beds.png',
        count: 1564,
        categories: categories),
    FurnitureType(
      id: 1,
      name: 'Ceiling',
      image: 'assets/images/ceiling_lamp.png',
      count: 1242,
      categories: categories,
    ),
    FurnitureType(
      id: 2,
      name: 'Lamps',
      image: 'assets/images/lamp.png',
      count: 1455,
      categories: categories,
    ),
    FurnitureType(
      id: 3,
      name: 'Furniture',
      image: 'assets/images/chair.png',
      count: 735,
      categories: categories,
    ),
    FurnitureType(
        id: 4,
        name: 'Beds',
        image: 'assets/images/beds.png',
        count: 1564,
        categories: categories),
  ];

  static final List<Furniture> furnitures = [
    Furniture('Teapot with black tea', 'assets/images/chair.png', '29.99'),
    Furniture('Table Wood Pine', 'assets/images/table.png', '35.00'),
    Furniture('Juliet Rowley Launge Sofa', 'assets/images/sofa.png', '77.99'),
    Furniture('Black And White Bed', 'assets/images/beds.png', '454.00'),
    Furniture('Teapot with black tea', 'assets/images/chair.png', '29.99'),
    Furniture('Table Wood Pine', 'assets/images/table.png', '35.00'),
    Furniture('Juliet Rowley Launge Sofa', 'assets/images/sofa.png', '77.99'),
    Furniture('Black And White Bed', 'assets/images/beds.png', '454.00'),
  ];
}
