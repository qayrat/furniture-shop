class Furniture {
  final String name;
  final String image;
  final String price;

  Furniture(this.name, this.image, this.price);
}

class Chair extends Furniture {
  Chair(super.name, super.image, super.price);
}
